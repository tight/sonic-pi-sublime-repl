require 'osc-ruby'
require 'cgi'

def read_from_repl
  eval($stdin.readline + $stdin.readline)
end

def write_to_repl(str)
  $stdout.puts str
  $stdout.flush
end

def send_to_sonic_pi(client, command, content = nil)
  if content
    client.send(OSC::Message.new(command, content))
  else
    client.send(OSC::Message.new(command))
  end
end

server_port = 4557
client = OSC::Client.new("localhost", server_port)

log_port = 4558
log = OSC::Server.new(log_port)

log.add_method '/info' do |message|
  write_to_repl message.to_a
end

log.add_method '/error' do |message|
  run, error, *backtrace = message.to_a
  error_message = CGI.unescapeHTML(error)

  write_to_repl "Error for run #{run}: #{error_message}"
end

Thread.new do
  log.run
end

sleep 3

while true
  code = read_from_repl

  if code
    if (code[0] == '/')
      send_to_sonic_pi(client, code.strip)
    else
      send_to_sonic_pi(client, '/run-code', code)
    end
  end

  sleep 1
end
