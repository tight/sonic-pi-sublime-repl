# Sonic Pi Sublime REPL

Quick & dirty way to use Sonic Pi within Sublime Text using Sublime REPL

## Install

Requires :

* Sonic Pi installed (https://github.com/samaaron/sonic-pi)
* SublimeREPL installed (https://sublimerepl.readthedocs.org)

Installation :

* copy `RubySonicPiLang` directory to `~/.config/sublime-text-3/Packages` (adds a new language for Ruby with Sonic Pi)
* copy `RubySonicPi` to `~/.config/sublime-text-3/Packages/SublimeREPL/config` (adds a new REPL for Ruby with Sonic Pi)
* edit `~/.config/sublime-text-3/Packages/SublimeREPL/text_transfer.py`, add below `ruby_sender` method :

```
@sender("ruby_sonic_pi")
def ruby_sonic_pi_sender(repl, text, view=None):
    code = binascii.b2a_base64(text.encode("utf-8"))
    payload = "begin require 'base64'; Base64.decode64('%s') end\n" % (code.decode("ascii"),)
    return default_sender(repl, payload, view)
```

## Usage

* start Sonic Pi server (in Sonic Pi dir, `app/server/bin/sonic-pi-server.rb`)
* in Sublime Text, set your code syntax to "Ruby / Sonic Pi" (View > Syntax / Ruby Sonic Pi)
* start Sublime REPL for Ruby / Sonic Pi (Tools > SublimeREPL > Ruby > Ruby / Sonic Pi), eventually split your window (View > Layout > Rows 2)
* use Sublime REPL shortcuts to send code (https://sublimerepl.readthedocs.org/en/latest/#source-buffer-keys)
* send selection (`Ctrl+, s`) with `/stop-all-jobs` to stop everything

## Todo

* autocompletion for samples, fx, parameters, ..
* support something else than my computer ^^
* keyboard shortcuts ?

## License

Sonic Pi Sublime REPL is released under the MIT License.